package com.xwintop.xJavaFxPlugIn.services.developTools;

import cn.hutool.core.thread.ThreadUtil;
import com.xwintop.xJavaFxPlugIn.controller.developTools.ScanPortToolController;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ScanPortToolService
 * @Description: 端口扫描工具
 * @author: xufeng
 * @date: 2019/5/15 17:34
 */

@Getter
@Setter
@Slf4j
public class ScanPortToolService {
    private ScanPortToolController scanPortToolController;

    public ScanPortToolService(ScanPortToolController scanPortToolController) {
        this.scanPortToolController = scanPortToolController;
    }

    public void scanAction() throws Exception {
        List<Integer> portsList = new ArrayList<>();
        for (Node child : scanPortToolController.getCommonPortFlowPane().getChildren()) {
            CheckBox checkBox = (CheckBox) child;
            if (checkBox.isSelected()) {
                String[] ports = checkBox.getText().split(",");
                for (String port : ports) {
                    if (port.contains(":")) {
                        portsList.add(Integer.valueOf(port.split(":")[1]));
                    } else {
                        portsList.add(Integer.valueOf(port));
                    }
                }
            }
        }
        String diyPortString = scanPortToolController.getDiyPortTextField().getText();
        if (StringUtils.isNotEmpty(diyPortString)) {
            for (String port : diyPortString.split(",")) {
                portsList.add(Integer.valueOf(port));
            }
        }
        log.info(portsList.toString());
        String host = scanPortToolController.getHostTextField().getText();
        InetAddress address = InetAddress.getByName(host); // 如果输入的是主机名，尝试获取ip地址
        scanPortToolController.getConnectStatusTableData().clear();
        for (Integer integer : portsList) {
            ThreadUtil.execute(() -> {
                Map<String, String> dataRow = new HashMap<String, String>();
                dataRow.put("port", integer.toString());
                try {
                    Socket socket = new Socket();// 定义套接字
                    SocketAddress socketAddress = new InetSocketAddress(address, integer);// 传递ip和端口
                    socket.connect(socketAddress, 1000);
                    // 对目标主机的指定端口进行连接，超时后连接失败
                    socket.close();
                    // 关闭端口
                    dataRow.put("status", "开放");
                    // 在文本区域里更新消息
                } catch (Exception e) {
                    dataRow.put("status", "关闭");
                }
                Platform.runLater(() -> {
                    scanPortToolController.getConnectStatusTableData().add(dataRow);
                });
            });
        }
    }
}
